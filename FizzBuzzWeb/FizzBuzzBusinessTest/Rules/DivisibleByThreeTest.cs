﻿namespace FizzBuzzBusinessTest.Rules
{
    using NUnit.Framework;
    using FizzBuzzBusiness.Rules;

    /// <summary>
    /// Class DivisibleByThreeTest.
    /// </summary>
    [TestFixture]
    public class DivisibleByThreeTest
    {
        /// <summary>
        /// Divisibilities the by three.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="result">if set to <c>true</c> [result].</param>
        [TestCase(9, true)]
        [TestCase(10, false)]
        public void DivisibilityByThree(int number, bool result)
        {
            // Arrange
            var fizz = new DivisibleByThree();

            // Act
            var display = fizz.IsDivisible(number);

            // Assert
            Assert.AreEqual(display, result);
        }

        /// <summary>
        /// Gets the fizz buzz.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="isSelectedDay">if set to <c>true</c> [is selected day].</param>
        [TestCase("Fizz", false)]
        [TestCase("Wizz", true)]
        public void GetFizzBuzz(string result, bool isSelectedDay)
        {
            // Arrange
            var fizz = new DivisibleByThree();

            // Act
            var display = fizz.GetFizzBuzz(isSelectedDay);

            // Assert
            Assert.IsNotNull(display);
            Assert.IsNotEmpty(display);
            Assert.AreEqual(display, result);
        }
    }
}
