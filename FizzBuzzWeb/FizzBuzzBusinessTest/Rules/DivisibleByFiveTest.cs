﻿namespace FizzBuzzBusinessTest.Rules
{
    using NUnit.Framework;
    using FizzBuzzBusiness.Rules;

    /// <summary>
    /// Class DivisibleByFiveTest.
    /// </summary>
    [TestFixture]
    public class DivisibleByFiveTest
    {
        /// <summary>
        /// Divisibilities Check by five.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="result">if set to <c>true</c> [result].</param>
        [TestCase(10, true)]
        [TestCase(9, false)]
        public void DivisibilityByFive(int number, bool result)
        {
            // Arrange
            var buzz = new DivisibleByFive();

            // Act
            var display = buzz.IsDivisible(number);

            // Assert
            Assert.AreEqual(display, result);
        }

        /// <summary>
        /// Gets the fizz buzz output.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="isSelectedDay">if set to <c>true</c> [is selected day].</param>
        [TestCase("Buzz", false)]
        [TestCase("Wuzz", true)]
        public void GetFizzBuzz(string result, bool isSelectedDay)
        {
            // Arrange
            var buzz = new DivisibleByFive();

            // Act
            var display = buzz.GetFizzBuzz(isSelectedDay);

            // Assert
            Assert.IsNotNull(display);
            Assert.IsNotEmpty(display);
            Assert.AreEqual(display, result);
        }
    }
}
