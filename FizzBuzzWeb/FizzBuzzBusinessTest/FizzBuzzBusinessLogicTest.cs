﻿namespace FizzBuzzBusinessTest
{
    using System;
    using System.Collections.Generic;
    using FizzBuzzBusiness.Contracts;
    using FizzBuzzBusiness;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class FizzBuzzBusinessLogicTest.
    /// </summary>
    [TestFixture]

    class FizzBuzzBusinessLogicTest
    {
        /// <summary>
        /// The fizz buzz business logic
        /// </summary>
        private FizzBuzzBusinessLogic fizzBuzzBusinessLogic;
        /// <summary>
        /// The divisibleby three mock
        /// </summary>
        private Mock<IDivisible> divisiblebyThreeMock;
        /// <summary>
        /// The divisibleby five mock
        /// </summary>
        private Mock<IDivisible> divisiblebyFiveMock;

        /// <summary>
        /// Setups this instance.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.divisiblebyThreeMock = new Mock<IDivisible>();
            this.divisiblebyFiveMock = new Mock<IDivisible>();
            divisiblebyThreeMock.Setup(x => x.IsDivisible(It.Is<int>(y => y % 3 == 0))).Returns(true);
            divisiblebyFiveMock.Setup(x => x.IsDivisible(It.Is<int>(y => y % 5 == 0))).Returns(true);
            divisiblebyThreeMock.Setup(x => x.GetFizzBuzz(It.Is<bool>(z => z))).Returns("Wizz");
            divisiblebyThreeMock.Setup(x => x.GetFizzBuzz(It.Is<bool>(z => !z))).Returns("Fizz");
            divisiblebyFiveMock.Setup(x => x.GetFizzBuzz(It.Is<bool>(z => z))).Returns("Wuzz");
            divisiblebyFiveMock.Setup(x => x.GetFizzBuzz(It.Is<bool>(z => !z))).Returns("Buzz");
        }


        /// <summary>
        /// Gets the list of fizz buzz numbers test.
        /// </summary>
        [Test]
        public void GetListofFizzBuzzNumbersTest()
        {
            // Arrange
            this.fizzBuzzBusinessLogic = new FizzBuzzBusinessLogic(new List<IDivisible> { divisiblebyThreeMock.Object, divisiblebyFiveMock.Object }, DateTime.Now.AddDays(1).DayOfWeek);
            var expectedlist = new List<string> { "1", "2", "Fizz", "4", "Buzz" };

            // Act
            var resultlist = fizzBuzzBusinessLogic.GetFizzBuzzList(5);

            // Assert
            Assert.AreEqual(expectedlist, resultlist);
        }

        /// <summary>
        /// Gets the list of wizz wuzz numbers test.
        /// </summary>
        [Test]
        public void GetListofWizzWuzzNumbersTest()
        {
            // Arrange
            this.fizzBuzzBusinessLogic = new FizzBuzzBusinessLogic(new List<IDivisible> { divisiblebyThreeMock.Object, divisiblebyFiveMock.Object }, DateTime.Now.DayOfWeek);
            var expectedlist = new List<string> { "1", "2", "Wizz", "4", "Wuzz" };

            // Act
            var resultlist = fizzBuzzBusinessLogic.GetFizzBuzzList(5);

            // Assert
            Assert.AreEqual(expectedlist, resultlist);
        }
    }
}
