﻿namespace FizzBuzzWebTest.Controllers
{
    using System;
    using System.Collections.Generic;
    using FizzBuzzBusiness.Contracts;
    using FizzBuzzWeb.Controllers;
    using FizzBuzzWeb.Models;
    using Moq;
    using NUnit.Framework;
    using System.Web.Mvc;

    /// <summary>
    /// Class FizzBuzzControllerTest.
    /// </summary>
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        /// <summary>
        /// Tests the type of what fizzbuzz Method Returns.
        /// </summary>
        [Test]
        public void TestFizzBuzzType()
        {
            // Arrange
            var controller = new FizzBuzzController(null);

            // Act
            var result = controller.FizzBuzz();

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(ViewResult), result);
        }

        /// <summary>
        /// Tests the invalid input range.
        /// </summary>
        [Test]
        public void TestInvalidInputRange()
        {
            // Arrange
            var fizzBuzzBusinessLogic = new Mock<IFizzBuzzBusinessLogic>();
            var controller = new FizzBuzzController(fizzBuzzBusinessLogic.Object);
            var viewModel = new FizzBuzzModel
            {
                Number = 3000
            };
            controller.ModelState.AddModelError("Number", new Exception());

            // Act
            var result = controller.FizzBuzz(viewModel) as ViewResult;

            // Assert
            Assert.AreEqual(result.ViewName, "FizzBuzz");
            Assert.AreEqual(viewModel.PagedList, null);
        }

        /// <summary>
        /// Tests the valid input.
        /// </summary>
        [Test]
        public void TestValidInput()
        {
            // Arrange
            var inputList = new List<string> { "1", "2", "Fizz", "4", "Buzz", "6", "7", "8", "9", "Buzz" };
            var fizzBuzzLogicMock = new Mock<IFizzBuzzBusinessLogic>();
            fizzBuzzLogicMock.Setup(x => x.GetFizzBuzzList(It.IsAny<int>())).Returns(inputList);
            var controller = new FizzBuzzController(fizzBuzzLogicMock.Object);
            var viewModel = new FizzBuzzModel
            {
                Number = 10
            };

            //Act
            var result = controller.FizzBuzz(viewModel) as ViewResult;
            var model = (FizzBuzzModel)result.ViewData.Model;

            //Assert
            Assert.IsNotNull(model);
            Assert.AreEqual(result.ViewName, "FizzBuzz");
            Assert.AreEqual(model.PagedList, inputList);
        }

        /// <summary>
        /// Tests the invalid input negative.
        /// </summary>
        [Test]
        public void TestInvalidInputNegative()
        {
            // Arrange
            var fizzBuzzBusinessLogicMock = new Mock<IFizzBuzzBusinessLogic>();
            var controller = new FizzBuzzController(fizzBuzzBusinessLogicMock.Object);
            var viewModel = new FizzBuzzModel
            {
                Number = -455
            };
            controller.ModelState.AddModelError("Number", new Exception());

            // Act
            var result = controller.FizzBuzz(viewModel) as ViewResult;

            // Assert
            Assert.AreEqual(result.ViewName, "FizzBuzz");
            Assert.AreEqual(viewModel.PagedList, null);
        }
    }
}
