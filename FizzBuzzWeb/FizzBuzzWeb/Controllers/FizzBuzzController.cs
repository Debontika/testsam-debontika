﻿namespace FizzBuzzWeb.Controllers
{
    using FizzBuzzBusiness.Contracts;
    using PagedList;
    using System.Web.Mvc;
    using Models;

    /// <summary>
    /// Class FizzBuzzController.
    /// </summary>
    /// <seealso cref="System.Web.Mvc.Controller" />
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// The page size
        /// </summary>
        private readonly int pageSize = 20;
        /// <summary>
        /// The fizz buzz business logic
        /// </summary>
        private readonly IFizzBuzzBusinessLogic fizzBuzzBusinessLogic;
        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzController"/> class.
        /// </summary>
        /// <param name="fizzBuzzBusinessLogic">The fizz buzz business logic.</param>
        public FizzBuzzController(IFizzBuzzBusinessLogic fizzBuzzBusinessLogic)
        {
            this.fizzBuzzBusinessLogic = fizzBuzzBusinessLogic;
        }

        /// <summary>
        /// FizzBuzz Get Method
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpGet]
        public ActionResult FizzBuzz()
        {
            return View(new FizzBuzzModel());
        }

        /// <summary>
        ///  FizzBuzz post Method
        /// </summary>
        /// <param name="fizzBuzzModel">The fizz buzz model.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult FizzBuzz(FizzBuzzModel fizzBuzzModel)
        {
            if (ModelState.IsValid)
            {
                var pageIndex = 1;
                fizzBuzzModel.PagedList = fizzBuzzBusinessLogic.GetFizzBuzzList(fizzBuzzModel.Number).ToPagedList(pageIndex, pageSize);
            }
            return View("FizzBuzz", fizzBuzzModel);
        }

        /// <summary>
        /// Pages the navigation.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="fizzBuzzModel">The fizz buzz model.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult PageNavigation(int page, FizzBuzzModel fizzBuzzModel)
        {
            var pageIndex = page;
            fizzBuzzModel.PagedList = fizzBuzzBusinessLogic.GetFizzBuzzList(fizzBuzzModel.Number).ToPagedList(pageIndex, pageSize);
            return View("FizzBuzz", fizzBuzzModel);
        }
    }
}