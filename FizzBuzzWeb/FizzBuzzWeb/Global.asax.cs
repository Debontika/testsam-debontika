﻿namespace FizzBuzzWeb
{
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using FizzBuzz;

    /// <summary>
    /// Class MvcApplication.
    /// </summary>
    /// <seealso cref="System.Web.HttpApplication" />
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Application start.
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
