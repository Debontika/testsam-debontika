﻿namespace FizzBuzzWeb.Models
{
    using PagedList;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Class FizzBuzzModel.
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Gets or sets the paged list.
        /// </summary>
        /// <value>The paged list.</value>
        public IPagedList<string> PagedList { get; set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>The number.</value>
        [Display(Name = "Number ")]
        [Required(ErrorMessage = "Enter a  Number")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Please enter a Positive Number")]
        [Range(1, 1000, ErrorMessage = "Please enter a positive Numbers ranging upto 1000 only")]
        public int Number { get; set; }
    }
}