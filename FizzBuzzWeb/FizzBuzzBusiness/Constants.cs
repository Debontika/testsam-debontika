﻿namespace FizzBuzzBusiness
{
    /// <summary>
    /// Class Constants.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// The divisible by three and not selected day
        /// </summary>
        public const string DivisibleByThreeAndNotSelectedDay = "Fizz";
        /// <summary>
        /// The divisible by three and selected day
        /// </summary>
        public const string DivisibleByThreeAndSelectedDay = "Wizz";
        /// <summary>
        /// The divisible by five and not selected day
        /// </summary>
        public const string DivisibleByFiveAndNotSelectedDay = "Buzz";
        /// <summary>
        /// The divisible by five and selected day
        /// </summary>
        public const string DivisibleByFiveAndSelectedDay = "Wuzz";
        /// <summary>
        /// The divisible by both and not selected day
        /// </summary>
        public const string DivisibleByBothAndNotSelectedDay = "Fizz Buzz";
        /// <summary>
        /// The divisible by both and selected day
        /// </summary>
        public const string DivisibleByBothAndSelectedDay = "Wizz Wuzz";
    }
}
