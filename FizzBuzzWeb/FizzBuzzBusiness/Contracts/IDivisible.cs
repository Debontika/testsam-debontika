﻿namespace FizzBuzzBusiness.Contracts
{
    /// <summary>
    /// Interface IDivisible
    /// </summary>
    public interface IDivisible
    {
        /// <summary>
        /// Gets the fizz buzz output.
        /// </summary>
        /// <param name="isDayCheckSatisfied">if set to <c>true</c> [is day check satisfied].</param>
        /// <returns>System.String.</returns>
        string GetFizzBuzz(bool isDayCheckSatisfied);

        /// <summary>
        /// Determines whether the specified value is divisible.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns><c>true</c> if the specified value is divisible; otherwise, <c>false</c>.</returns>
        bool IsDivisible(int value);
    }
}
