﻿namespace FizzBuzzBusiness.Contracts
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface IFizzBuzzBusinessLogic
    /// </summary>
    public interface IFizzBuzzBusinessLogic
    {
        /// <summary>
        /// Gets the fizz buzz list.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>IEnumerable&lt;System.String&gt;.</returns>
        IEnumerable<string> GetFizzBuzzList(int number);
    }
}
