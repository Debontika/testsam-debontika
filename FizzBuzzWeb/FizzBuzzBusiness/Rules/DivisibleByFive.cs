﻿namespace FizzBuzzBusiness.Rules
{
    using Contracts;

    /// <summary>
    /// Class DivisibleByFive.
    /// </summary>
    /// <seealso cref="FizzBuzzBusiness.Contracts.IDivisible" />
    public class DivisibleByFive : IDivisible
    {
        /// <summary>
        /// Gets the fizz buzz output.
        /// </summary>
        /// <param name="isDayCheckSatisfied">if set to <c>true</c> [is day check satisfied].</param>
        /// <returns>System.String.</returns>
        public string GetFizzBuzz(bool isDayCheckSatisfied)
        {
            return isDayCheckSatisfied
                ? Constants.DivisibleByFiveAndSelectedDay
                : Constants.DivisibleByFiveAndNotSelectedDay;

        }

        /// <summary>
        /// Determines whether the specified value is divisible.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns><c>true</c> if the specified value is divisible; otherwise, <c>false</c>.</returns>
        public bool IsDivisible(int value)
        {
            return (value % 5) == 0;
        }
    }
}

