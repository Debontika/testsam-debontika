﻿namespace FizzBuzzBusiness
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Contracts;

    /// <summary>
    /// Class FizzBuzzBusinessLogic.
    /// </summary>
    /// <seealso cref="FizzBuzzBusiness.Contracts.IFizzBuzzBusinessLogic" />
    public class FizzBuzzBusinessLogic : IFizzBuzzBusinessLogic
    {
        /// <summary>
        /// The division list
        /// </summary>
        private readonly IEnumerable<IDivisible> divisionList;

        /// <summary>
        /// The day of week
        /// </summary>
        private readonly DayOfWeek dayOfWeek;

        /// <summary>
        /// Gets a value indicating whether this instance is day check satisfied.
        /// </summary>
        /// <value><c>true</c> if this instance is day check satisfied; otherwise, <c>false</c>.</value>
        private bool isDayCheckSatisfied => DateTime.Today.DayOfWeek == this.dayOfWeek;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzBusinessLogic"/> class.
        /// </summary>
        /// <param name="divisionList">The division list.</param>
        /// <param name="dayOfWeek">The day of week.</param>
        public FizzBuzzBusinessLogic(IEnumerable<IDivisible> divisionList, DayOfWeek dayOfWeek)
        {
            this.divisionList = divisionList;
            this.dayOfWeek = dayOfWeek;
        }

        /// <summary>
        /// Gets the fizz buzz list.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>IEnumerable&lt;System.String&gt;.</returns>
        public IEnumerable<string> GetFizzBuzzList(int number)
        {
            var list = new List<string>();

            for (var i = 1; i <= number; i++)
            {
                var result = divisionList.Select(x => x.IsDivisible(i) ? x.GetFizzBuzz(this.isDayCheckSatisfied) : null)
                    .Where(x => !string.IsNullOrEmpty(x)).ToList();
                var output = string.Join(" ", result);
                if (string.IsNullOrEmpty(output))
                {
                    output = i.ToString();
                }

                list.Add(output);
            }
            return list;
        }
    }
}
